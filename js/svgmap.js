var svgViloyatlar = document.querySelectorAll("#viloyatlar > *");
var viloyatNomlari = document.querySelectorAll(".viloyat li");
var activeViloyat = document.querySelector(".viloyat li.active");

function removeAllOn() {
  viloyatNomlari.forEach(function(el) {
    el.classList.remove("on");
  });
  svgViloyatlar.forEach(function(el) {
    el.classList.remove("on");
  });
  activeViloyat.classList.add("active");
}

function addOnFromState(el) {
  let viloyatID = el.getAttribute("id");
  let curViloyat = document.querySelector("[data-state='" + viloyatID + "']");
  el.classList.add("on");
  curViloyat.classList.add("on");
  activeViloyat = document.querySelector(".viloyat li.active");
  activeViloyat.classList.remove("active");
}

svgViloyatlar.forEach(function(el) {
  el.addEventListener("mouseenter", function() {
    addOnFromState(el);
  });
  el.addEventListener("mouseleave", function() {
     removeAllOn();
  });
  
  el.addEventListener("touchstart", function() {
    removeAllOn();
    addOnFromState(el);
  });
});

function removeAllActive() {
  svgViloyatlar.forEach(function(el) {
    el.classList.remove("active");
  });
}

function setViloyat(el) {
	removeAllActive();
	el.classList.add("active");
	
	let viloyatID = el.getAttribute("id");
	let curViloyat = document.querySelector("[data-state='" + viloyatID + "']");
	activeViloyat.classList.remove("active");
	activeViloyat = curViloyat;
	activeViloyat.classList.add("active");
	
	svgInfo();
}

/*----------------------------------------------------------------------------------------------------------------------*/
var mapinfo = {
	toshkent: [
		{filial:"Yashnobod filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Mirzo Ulug'bek filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Olmazor filiali", mfo:"00876", tel:" (71) 228-78-52", address:"100178,Toshkent sh., Olmazor t., Qoraqamich 2 k., 54", logo:"icons/logo/nbu.svg" }
		],
	toshkentv: [
		{filial:"Bokobod filiali", mfo:"00912", tel:"(0-370) 602-80-09", address:"102911, Bekobod sh., Peshakov k., 22", logo:"icons/logo/nbu.svg" },
		{filial:"Angren filiali", mfo:"00890", tel:"(0-372) 222-38-27", address:"102500, Angren sh., Ohangaron k, 1", logo:"icons/logo/nbu.svg" }
		],
	xorazm: [
		{filial:"Hazorasp filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"SHovot filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	sirdaryo: [
		{filial:"Oqoltin tumani", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Mirzaobod tumani", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	buxoro: [
		{filial:"G'ijduvon filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Kogon filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	qashqadaryo: [
		{filial:"Muborak filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"G'uzor filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	fargona: [
		{filial:"Oltiariq tumani", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Qo'qon filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	jizzax: [
		{filial:"Mirzacho'l filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Paxtakor tumani", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	navoiy: [
		{filial:"Zarafshon filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Uchquduq filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	samarqand: [
		{filial:"Urgut filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Pastdarg'om filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	surxondaryo: [
		{filial:"Boysun tumani", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Denov tumani", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	andijon: [
		{filial:"Asaka filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Marxamat filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	namangan: [
		{filial:"Chortoq filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"Turaqo'rg'on filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		],
	qoraqolgiston: [
		{filial:"Qo'ng'irot filiali", mfo:"00847", tel:"(71) 233 19 94", address:"700016,Toshkent sh., Yashnobod t, S. Azimov k, 2a-uy, \"Uspenskaya\" bekati", logo:"icons/logo/nbu.svg" },
		{filial:"To'rtko'l filiali", mfo:"00895", tel:"(71) 237-04-86", address:"100000, Toshkent sh, M. Ulug'bek t, Mustaqillik k, 66, \"H. Alimjon\" metro bekati", logo:"icons/logo/nbu.svg" }
		]
};
/*-------------------------------------------------------------------------------------------------------------------------------*/

function svgInfo() {
	let curViloyat, data;
	curViloyat = activeViloyat.getAttribute('data-state');
	data = mapinfo[curViloyat];
	drawInfo(data);
}

function drawInfo(data) {
	let len, code = "";
	len = data.length;
	
	if(len > 0) {
		for(var i=0; i < len; i++) {
			code += `<div class="svginfo-item"><div class="svginfo-title">
					<img src=` + data[i].logo + ` />
					<span>` + data[i].filial + `</span>
				</div>
				<div class="svginfo-text">
					<span>MFO:</span>
					<p>` + data[i].mfo + `</p>
				</div>
				<div class="svginfo-text">
					<span>Tel:</span>
					<p>` + data[i].tel + `</p>
				</div>
				<div class="svginfo-text">
					<span>Manzil:</span>
					<p>` + data[i].address + `</p>
				</div>							
			</div>`;
		}
	} else {
		code = "";
	}
	
	console.log(code);
	document.getElementById("svginfo").innerHTML = code;
}

document.addEventListener('DOMContentLoaded', function(){
	svgInfo();
});